import 'package:test/test.dart';
import 'package:tnt_model/mapper/mapper.dart';
import 'package:tnt_model/tnt_model.dart';


void main() {
  var answer1 = Answer(html: 'Test answer 1', correct: true, id: 'x');
  var answer2 = Answer(html: 'Test answer 2', correct: false);
  var question1 = Question(
      name: 'Test question 1',
      html: 'Test question 1',
      note: 'note 1',
      questionType: QuestionTypes.simple,
      answersList: [answer1, answer2]);
  var question2 = Question(
      name: 'Test question 2',
      html: 'Test question 2',
      note: 'note 2',
      questionType: QuestionTypes.simple,
      answersList: [answer1, answer2]);
  var topic1 = Topic(
      name: 'Test topic 1',
      html: 'Test topic 1',
      questions: [question1, question2]);
  var topic2 = Topic(
      name: 'Test topic 2',
      html: 'Test topic 2',
      questions: [question1, question2]);
  var course1 =
      Course(name: 'Course 1', html: 'Course 1', topics: [topic1, topic2]);
  var course2 =
      Course(name: 'Course 2', html: 'Course 2', topics: [topic1, topic2]);
  var mapper = Mapper();
  group('Mapper tests', () {
    setUp(() {
      // Additional setup goes here.
    });
    test('class Answer test', () {
      var map = mapper.answerToMap(answer1);
      var answer11 = mapper.mapToAnswer(map);
      expect(answer11, answer1);
    });
    test('class Question test', () {
      var map = mapper.questionToMap(question1);
      var question11 = mapper.mapToQuestion(map);
      expect(question11, question1);
    });
    test('class Topic test', () {
      var map = mapper.topicToMap(topic1);
      var topic11 = mapper.mapToTopic(map);
      expect(topic11, topic1);
    });
    test('class Course test', () {
      var map = mapper.courseToMap(course1);
      var course11 = mapper.mapToCourse(map);
      expect(course11, course1);
    });
  });
  group('Packet tests', () {
    var packet = Packet.makePacket();
    packet.addCourse(course1);
    packet.addCourse(course2);
    test('Packet toJson-FromJson test', () {
      var jsonString = packet.toJsonString();
      var result = Packet.makePacketFromJson(jsonString);
      expect(result.isOk, true);
      var packetNew = result.data as Packet;
      var expectCourses = packet.getCourses().data as List<Course>;
      var matcher = containsAll(expectCourses);
      var actualCourses = packetNew.getCourses().data as List<Course>;
      expect(actualCourses, matcher);
    });
  });

  test('check courses with topics equality', () {
    // В [c1] и [c2] передан один и тот же объект!
    final c1 = Course(name: 'Course 1', html: 'Course 1', topics: [topic1]);
    final c2 = Course(name: 'Course 1', html: 'Course 1', topics: [topic1]);
    expect(c1, equals(c2));
    final c3 = c2.copyWith(topics: [topic2]);
    expect(c1, isNot(equals(c3)));

    // Внимание! Проверка, что "изменив" объект [topic1] в [c2] мы на самом деле
    // никак не сломали его [copy-by-reference] в [c1],
    // так как на самом деле изменения не было - была создана новая копия
    final c4 = c2
        .copyWith(topics: [c2.topics.elementAt(0).copyWith(name: 'new name')]);
    expect(c4.topics.elementAt(0).name, equals('new name'));
    expect(c1.topics.elementAt(0).name, isNot(equals('new name')));
  });
  test('check constructor', () {
    var answer3 = Answer(html: 'answer3', correct: true);
    var list = [answer1, answer2];
    var questionXXX = Question(
        html: 'html',
        name: 'name',
        note: 'note',
        questionType: QuestionTypes.simple,
        answersList: list);
    expect(() => questionXXX.answers.add(answer3),
        throwsA(isA<UnsupportedError>()));
    expect(2, questionXXX.answers.length);
  });
  test('Course test', (){
    var course = testData();
    print('''course name: ${course.name}
    course html: ${course.html}''');
    for(var t in course.topics){
      print('''\ttopic name: ${t.name}
      topic html: ${t.html}''');
      for(var q in t.questions){
        print('''\t\tquery name: ${q.name}
        query html: ${q.html}''');
        for(var a in q.answers){
          print('''\t\t\tanswer html:${a.html}
          answer is correct: ${a.correct}''');
        }
      }
    }
  });
}
