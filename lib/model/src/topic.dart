import 'package:equatable/equatable.dart';

import 'question.dart';

class Topic extends Equatable {
  Topic({
    required this.name,
    required this.html,
    required questions,
    this.id = '',
  }) : questions = List.unmodifiable(questions);

  final String name;
  final String html;
  final List<Question> questions;
  final String id;

  Topic copyWith({
    String? name,
    String? html,
    List<Question>? questions,
    String? id,
  }) {
    if ((name == null || identical(name, this.name)) &&
        (html == null || identical(html, this.html)) &&
        (questions == null || identical(questions, this.questions)) &&
        (id == null || identical(id, this.id))) {
      return this;
    }

    return Topic(
      name: name ?? this.name,
      html: html ?? this.html,
      questions: questions ?? this.questions,
      id: id ?? this.id,
    );
  }

  @override
  List<Object?> get props => [name, html, questions, id];
}
