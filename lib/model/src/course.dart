import 'package:equatable/equatable.dart';
import 'topic.dart';

class Course extends Equatable {
  Course({
    required this.name,
    required this.html,
    required topics,
    this.id = '',
    this.version = 0
  }) : topics = List.unmodifiable(topics);



  final String name;
  final String html;
  final List<Topic> topics;
  final String? id;
  final int version;

  @override
  List<Object?> get props => [name, html, id, topics, version];

  Course copyWith({
    String? name,
    String? html,
    List<Topic>? topics,
    String? id,
    int? version,
  }) {
    return Course(
      name: name ?? this.name,
      html: html ?? this.html,
      topics: topics ?? this.topics,
      id: id ?? this.id,
      version: version ?? this.version,
    );
  }
}
