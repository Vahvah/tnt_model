import 'package:equatable/equatable.dart';
import 'answer.dart';

/*
var list = getAnswers();
var question = Question('', '', '', xxx, list);
list.add(Answer(....));
* */

enum QuestionTypes { simple, multiAnswered }

class Question extends Equatable {
  final String id;
  final String html;
  final String name;
  final String note;
  final QuestionTypes questionType;
  final List<Answer> answers;

  Question copyWith({
    String? id,
    String? html,
    String? name,
    String? note,
    QuestionTypes? questionType,
    List<Answer>? answers,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (html == null || identical(html, this.html)) &&
        (name == null || identical(name, this.name)) &&
        (questionType == null || identical(questionType, this.questionType)) &&
        (answers == null || identical(answers, this.answers)) &&
        (note == null || identical(note, this.note))) {
      return this;
    }

    return Question(
      id: id ?? this.id,
      html: html ?? this.html,
      name: name ?? this.name,
      note: note ?? this.note,
      questionType: questionType ?? this.questionType,
      answersList: answers ?? this.answers,
    );
  }

  Question(
      {this.id = '',
      required this.html,
      required this.name,
      required this.note,
      required this.questionType,
      required List<Answer> answersList})
      : answers = List.unmodifiable(answersList);

  /*  Question(
      {required String question_name,
      required String question_html,
      required QuestionTypes type,
      required List<Answer> answers_list,
      String question_id = ''})
      : name = question_name,
        html = question_html,
        questionType = type,
        id = question_id {
    answers.addAll(answers_list);
  }*/

  @override
  List<Object> get props => [id, html, name, note, questionType, answers];
}
