import 'package:equatable/equatable.dart';

class Account extends Equatable{
  final String id;

  @override
  List<Object> get props => [id];

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is Account &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => super.hashCode ^ id.hashCode;

  Account({this.id = 'guest'});

  Account copyWith({
    String? id,
  }) {
    return Account(
      id: id ?? this.id,
    );
  }
}