/*
* Class for answers.
* text - text representation for user
* correct - ture if Answer is correct
* */
import 'package:equatable/equatable.dart';

class Answer extends Equatable {
  final String id;
  final String html;
  final bool correct;

  Answer copyWith({
    String? id,
    String? html,
    bool? correct,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (html == null || identical(html, this.html)) &&
        (correct == null || identical(correct, this.correct))) {
      return this;
    }

    return Answer(
      id: id ?? this.id,
      html: html ?? this.html,
      correct: correct ?? this.correct,
    );
  }

  Answer({required this.html, required this.correct, this.id = ''});
  /*
  @override
  bool operator ==(Object other) {
    if(other is! Answer){
      return false;
    }
    if(id != other.id){
      return false;
    }
    if(html != other.html){
      return false;
    }
    if(correct != other.correct){
      return false;
    }
    return true;
//    return super == other;
  }*/

  @override
  List<Object> get props => [id, html, correct];
}
