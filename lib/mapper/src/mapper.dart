import 'package:tnt_model/tnt_model.dart';

class Mapper {
  static const id = 'id';
  static const html = 'html';
  static const correct = 'correct';
  static const name = 'name';
  static const questionType = 'questionType';
  static const answers = 'answers';
  static const questions = 'questions';
  static const topics = 'topics';
  static const version = 'version';
  static const note = 'note';

  Map<String, dynamic> answerToMap(Answer answer) {
    var map = <String, dynamic>{};
    map[id] = answer.id;
    map[html] = answer.html;
    map[correct] = answer.correct;
    return map;
  }

  Answer mapToAnswer(Map<String, dynamic> map) {
    var aId = map[id];
    if (aId == null) {
      throw FormatException('''Mapper.mapToAnswer field 'id' is missed''');
    }
    if (aId is! String) {
      throw FormatException('''Mapper.mapToAnswer field 'id' has wrong type''');
    }
    var aHtml = map[html];
    if (aHtml == null) {
      throw FormatException('''Mapper.mapToAnswer field 'html' is missed''');
    }
    if (aHtml is! String) {
      throw FormatException(
          '''Mapper.mapToAnswer field 'html' has wrong type''');
    }
    var aCorrect = map[correct];
    if (aCorrect == null) {
      throw FormatException('''Mapper.mapToAnswer field 'correct' is missed''');
    }
    if (aCorrect is! bool) {
      throw FormatException(
          '''Mapper.mapToAnswer field 'correct' has wrong type''');
    }
    return Answer(html: aHtml, correct: aCorrect, id: aId);
  }

  Map<String, dynamic> questionToMap(Question question) {
    var map = <String, dynamic>{};
    map[id] = question.id;
    map[html] = question.html;
    map[name] = question.name;
    map[note] = question.note;
    map[questionType] = question.questionType.toString();
    var list = List<Map<String, dynamic>>.empty(growable: true);
    for (var a in question.answers) {
      list.add(answerToMap(a));
    }
    map[answers] = list;
    return map;
  }

  Question mapToQuestion(Map<String, dynamic> map) {
    var qId = map[id];
    if (qId == null) {
      throw FormatException('''Mapper.mapToQuestion field 'id' is missed''');
    }
    if (qId is! String) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'id' has wrong type''');
    }
    var qHtml = map[html];
    if (qHtml == null) {
      throw FormatException('''Mapper.mapToQuestion field 'html' is missed''');
    }
    if (qHtml is! String) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'html' has wrong type''');
    }
    var qName = map[name];
    if (qName == null) {
      throw FormatException('''Mapper.mapToQuestion field 'name' is missed''');
    }
    if (qName is! String) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'name' has wrong type''');
    }
    var qType = map[questionType];
    if (qType == null) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'questionType' is missed''');
    }
    var qNote = map[note];
    if(qNote == null){
      throw FormatException(
          '''Mapper.mapToQuestion field 'note' is missed''');
    }
    if(qNote is! String){
      throw FormatException(
          '''Mapper.mapToQuestion field 'note' has wrong type''');
    }
    if (qType is! String) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'questionType' has wrong type''');
    }
    var type = QuestionTypes.values.firstWhere(
        (element) => element.toString() == qType,
        orElse: () => throw FormatException(
            '''Mapper.mapToQuestion field 'questionType' has wrong value'''));
    var list = map[answers];
    if (list == null) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'answers' is missed''');
    }
    if (list is! List<dynamic>) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'answers' has wrong type''');
    }
    try {
      list = list.cast<Map<String, dynamic>>();
    } catch (e) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'answers' can`t cast to Map<String, dynamic>''');
    }
    var qAnswers = List<Answer>.empty(growable: true);
    for (var aMap in list) {
      qAnswers.add(mapToAnswer(aMap));
    }
    return Question(
        name: qName,
        html: qHtml,
        note: qNote,
        questionType: type,
        answersList: qAnswers,
        id: qId);
  }

  Map<String, dynamic> topicToMap(Topic topic) {
    var map = <String, dynamic>{};
    map[id] = topic.id;
    map[html] = topic.html;
    map[name] = topic.name;
    var list = List<Map<String, dynamic>>.empty(growable: true);
    for (var q in topic.questions) {
      list.add(questionToMap(q));
    }
    map[questions] = list;
    return map;
  }

  Topic mapToTopic(Map<String, dynamic> map) {
    var tId = map[id];
    if (tId == null) {
      throw FormatException('''Mapper.mapToTopic field 'id' is missed''');
    }
    if (tId is! String) {
      throw FormatException('''Mapper.mapToTopic field 'id' has wrong type''');
    }
    var tHtml = map[html];
    if (tHtml == null) {
      throw FormatException('''Mapper.mapToTopic field 'html' is missed''');
    }
    if (tHtml is! String) {
      throw FormatException(
          '''Mapper.mapToTopic field 'html' has wrong type''');
    }
    var tName = map[name];
    if (tName == null) {
      throw FormatException('''Mapper.mapToTopic field 'name' is missed''');
    }
    if (tName is! String) {
      throw FormatException(
          '''Mapper.mapToTopic field 'name' has wrong type''');
    }
    var list = map[questions];
    if (list == null) {
      throw FormatException(
          '''Mapper.mapToTopic field 'questions' is missed''');
    }
    if (list is! List<dynamic>) {
      throw FormatException(
          '''Mapper.mapToTopic field 'questions' has wrong type''');
    }
    try {
      list = list.cast<Map<String, dynamic>>();
    } catch (e) {
      throw FormatException(
          '''Mapper.mapToQuestion field 'questions' can`t cast to Map<String, dynamic>''');
    }
    var tQuestions = List<Question>.empty(growable: true);
    for (var aMap in list) {
      tQuestions.add(mapToQuestion(aMap));
    }
    return Topic(name: tName, html: tHtml, questions: tQuestions, id: tId);
  }

  Map<String, dynamic> courseToMap(Course course) {
    var map = <String, dynamic>{};
    map[id] = course.id;
    map[html] = course.html;
    map[name] = course.name;
    map[version] = course.version;
    var list = List<Map<String, dynamic>>.empty(growable: true);
    for (var t in course.topics) {
      list.add(topicToMap(t));
    }
    map[topics] = list;
    return map;
  }

  Course mapToCourse(Map<String, dynamic> map) {
    var cId = map[id];
    if (cId == null) {
      throw FormatException('''Mapper.mapToCourse field 'id' is missed''');
    }
    if (cId is! String) {
      throw FormatException('''Mapper.mapToCourse field 'id' has wrong type''');
    }
    var cHtml = map[html];
    if (cHtml == null) {
      throw FormatException('''Mapper.mapToCourse field 'html' is missed''');
    }
    if (cHtml is! String) {
      throw FormatException(
          '''Mapper.mapToCourse field 'html' has wrong type''');
    }
    var cName = map[name];
    if (cName == null) {
      throw FormatException('''Mapper.mapToCourse field 'name' is missed''');
    }
    if (cName is! String) {
      throw FormatException(
          '''Mapper.mapToCourse field 'name' has wrong type''');
    }
    var cVersion = map[version];
    if(cVersion == null){
      throw FormatException('''Mapper.mapToCourse field 'version' is missed''');
    }
    if (cVersion is! int) {
      throw FormatException(
          '''Mapper.mapToCourse field 'version' has wrong type''');
    }
    var list = map[topics];
    if (list == null) {
      throw FormatException('''Mapper.mapToCourse field 'topics' is missed''');
    }
    if (list is! List<dynamic>) {
      throw FormatException(
          '''Mapper.mapToCourse field 'topics' has wrong type''');
    }
    try {
      list = list.cast<Map<String, dynamic>>();
    } catch (e) {
      throw FormatException(
          '''Mapper.mapToCourse field 'topics' can`t cast to List<Map<String,dynamic>>''');
    }
    var cTopics = List<Topic>.empty(growable: true);
    for (var tMap in list) {
      cTopics.add(mapToTopic(tMap));
    }
    return Course(html: cHtml, name: cName, topics: cTopics, id: cId);
  }

  Map<String, dynamic> accountToMap(Account account){
    var map = <String, dynamic>{};
    map[id] = account.id;
    return map;
  }
  Account mapToAccount(Map<String, dynamic> map){
    var aId = map[id];
    if (aId == null) {
      throw FormatException('''Mapper.mapToAccount field 'id' is missed''');
    }
    if (aId is! String) {
      throw FormatException('''Mapper.mapToAccount field 'id' has wrong type''');
    }
    return Account(id: aId);
  }
}
