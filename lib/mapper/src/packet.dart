import 'dart:convert';
import 'package:tnt_model/model/src/account.dart';
import 'package:tnt_model/model/src/course.dart';
import 'mapper.dart';

abstract class Packet {
  static final packet_version = 'version';
  static final instances = 'instances';
  static final instance = 'instance';
  static final instance_type = 'instance_type';
  static final type_course = 'Course';
  static final type_account = 'Account';
  static final packets = [Packet_0()];
  static Result makePacketFromJson(String json) {
    try {
      var decoder = JsonDecoder();
      var map = decoder.convert(json);
      if (map is! Map<String, dynamic>) {
        return Result(false, null, 'Wrong format');
      }
      var version = map[packet_version];
      if (version == null || version is! int) {
        return Result(false, null, 'Wrong format');
      }
      if (version >= packets.length) {
        return Result(false, null, 'Packet version is not supported');
      }
      var packet = packets[version].makeSame();
      packet._map.addAll(map);
      return Result(true, packet, '');
    } catch (e) {
      return Result(false, null, e.toString());
    }
  }

  String toJsonString() {
    return JsonEncoder().convert(_map);
  }

  static Packet makePacket([int version = -1]) {
    if (version == -1 || version >= packets.length) {
      return packets.last.makeSame();
    }
    return packets[version].makeSame();
  }

  final _map = <String, dynamic>{};
  Packet makeSame(); //Make packet with the same version
  int get version;
  int get lastVersion => packets.last.version;
  bool addCourse(Course course);
  Result getCourses();
  bool addAccount(Account account);
  Account getAccount();
  void _initPacket() {
    _map[packet_version] = version;
    _map[instances] = <Map<String, dynamic>>[];
  }
}

class Packet_0 extends Packet {
  Packet_0() {
    _initPacket();
  }
  @override
  int get version => 0;
  @override
  Packet makeSame() => Packet_0();
  @override
  bool addCourse(Course course) {
    var list = _map[Packet.instances];
    if (list == null) {
      return false;
    }
    try {
      list as List<Map<String, dynamic>>;
      var mapper = Mapper();
      var map = <String, dynamic>{};
      map[Packet.instance_type] = Packet.type_course;
      map[Packet.instance] = mapper.courseToMap(course);
      list.add(map);
      return true;
    } catch (e) {
      return false;
    }
  }

  @override
  Result getCourses() {
    try {
      var courses = <Course>[];
      var instances = _map[Packet.instances];
      var mapper = Mapper();
      for (var map in instances) {
        var type = map[Packet.instance_type];
        if (type == null) {
          return Result(
              false, null, 'Packet_0.getCourses instance_type not founded');
        }
        if (type != Packet.type_course) {
          continue;
        }
        var i = map[Packet.instance];
        if (i == null) {
          return Result(
              false, null, 'Packet_0.getCourses instance not founded');
        }
        courses.add(mapper.mapToCourse(i));
      }
      return Result(true, courses, '');
    } catch (e) {
      return Result(false, null, e.toString());
    }
  }
  @override
  bool addAccount(Account account){
    var list = _map[Packet.instances];
    if (list == null) {
      return false;
    }
    var map = <String, dynamic>{};
    map[Packet.instance_type] = Packet.type_account;
    var mapper = Mapper();
    map[Packet.instance] = mapper.accountToMap(account);
    list as List<Map<String, dynamic>>;
    list.add(map);
    return true;
  }
  @override
  Account getAccount(){
    var instances = _map[Packet.instances];
    var mapper = Mapper();
    for (var map in instances) {
      var type = map[Packet.instance_type];
      if (type == null) {
        return Account();
      }
      if (type != Packet.type_account) {
        continue;
      }
      var i = map[Packet.instance];
      if (i == null) {
        return Account();
      }
      var account = mapper.mapToAccount(map);
      return account;
    }
    return Account();
  }
}
class Result {
  final bool _ok;
  bool get isOk => _ok;
  dynamic data;
  String status;
  Result(this._ok, this.data, this.status);
}
